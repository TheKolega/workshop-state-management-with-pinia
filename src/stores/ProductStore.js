import { defineStore, acceptHMRUpdate } from "pinia";

export const useProductStore = defineStore("ProductStore", {
  state: () => {
    return {
      products: [],
    };
  },
  actions: {
    async fill() {
      const res = await fetch("/products.json");
      this.products = await res.json();
    },
  },
  getters: {
    count: (state) => state.products.length,
    isEmpty: (state) => state.count === 0,
    productByName: (state) => (name) => {
      return state.products.find((product) => product.name === name);
    },
    productById: (state) => (id) => {
      return state.products.find((product) => product.id === id);
    },
    productPrice: (state) => (productId) => {
      return state.productById(productId).price;
    },
  },
});

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useProductStore, import.meta.hot));
}

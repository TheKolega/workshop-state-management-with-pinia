import { defineStore, acceptHMRUpdate } from "pinia";
import { useProductStore } from "@/stores/ProductStore";

export const useCartStore = defineStore("CartStore", {
  state: () => {
    return {
      items: [],
    };
  },
  actions: {
    async fill() {
      const res = await fetch("/cart.json");
      this.items = await res.json();
    },
    addItem(itemId, count) {
      if (count !== 0) {
        const item = this.items.find((item) => {
          if (item.id === itemId) return item;
        });
        if (item) {
          item.count += count;
        } else {
          this.items.push({ id: itemId, count: count });
        }
      }
    },
  },
  getters: {
    count: (state) => state.items.length,
    isEmpty: (state) => state.count === 0,
    itemsSum: (state) => {
      let sum = 0;
      if (state.items) {
        for (let item of state.items) {
          sum += item.count;
        }
      }
      return sum;
    },
    itemPricesSum: (state) => {
      let total = 0;
      if (state.items) {
        const productStore = useProductStore();
        for (let item of state.items) {
          total += productStore.productPrice(item.id) * item.count;
        }
      }
      return total;
    },
  },
});

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useCartStore, import.meta.hot));
}

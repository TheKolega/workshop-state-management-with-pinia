import { createApp } from "vue";
import App from "./App.vue";
import BoilerplatePlugin from "./plugins/BoilerplatePlugin";
import { createPinia } from "pinia";

// Init Pinia
const pinia = createPinia();

// Init App
const app = createApp(App);
app.use(BoilerplatePlugin);
app.use(pinia);

app.mount("#app");
